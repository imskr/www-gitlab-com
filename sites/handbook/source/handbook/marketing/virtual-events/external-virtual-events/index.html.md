---
layout: handbook-page-toc
title: External Virtual Events
description: An overview of external virtual events including virtual conferences where we sponsor a booth, and sponsored webinars with third party vendors.
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# External Virtual Events Overview
{:.no_toc}
---

## Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

External virtual events are, by definition, not owned and hosted by GitLab. They are hosted by an external third party (i.e. a partner or paid vendor). The goal of external virtual events is to drive net new leads, and we do not promote to our internal database. The various types of external virtual events are below, and involve epic and issue creation, designation of DRIs, and workback schedule definition within the issue due dates.

* [Partner-Hosted Webinars](/handbook/marketing/virtual-events/external-virtual-events/#partner-hosted-webinars): hosted by a channel partner (i.e. WWT), this is an unpaid tactic. The channel partner manages landing page, moderating and hosting the webinar on their platform. GitLab represents as a speaker at the event, sometimes jointly with an alliance partner. A lead list is often not shared after the event, as the channel partner will work the leads. We will sometimes promote, and determine which channels are appropriate.
* [Sponsored Webinars](): hosted on an external vendor platform (i.e. DevOps.com), this is a paid tactic. The vendor is responsible for driving registration, moderating and hosting the webinar on their platform, and delivering a lead list after the event. The goal of a sponsored webinar is net new leads - we do not promote to our existing database as it is a paid activity.
* [Virtual Conferences](): hosted on an external vendor platform (i.e. Hopin), this is a paid tactic. GitLab pays a sponsorship fee to receive a virtual booth and often speaking session or panel presence. The goal of a sponsored virtual conference is net new leads - we do not promote to our existing database as it is a paid activity.
* [Executive Roundtable](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-executive-roundtable):  hosted on an external vendor platform, this is a gathering of high level CxO attendees run as an open discussion between the moderator/host, GitLab expert and delegates. There usually aren't any presentations, but instead a discussion where anyone can chime in to speak. The host would prepare questions to lead discussion topics and go around the room asking delegates questions to answer. The goal of an executive roundtable is net new leads - we do not promote to our existing database as it is a paid activity.
* [Vendor Arranged Meetings](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-vendor-arranged-meetings): hosted by an external vendor, the vendor organizes one-to-one meetings with prospect or customer accounts. This does not include meetings set internally by GitLab team members. An example would be a "speed dating" style meeting setup where a vendor organizes meetings with prospects of interest to GitLab. The goal of a venor arranged meeting is to generate meetings with accounts of interest that we are finding challenging to break into directly - we do not promote to our existing database as it is a paid activity.

## Partner Webinar
{: #partner-hosted-webinars .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

*A sponsored webinar is hosted on an external vendor platform (e.g: DevOps.com), with the goal of driving net new leads. The vendor is responsible for driving registration, moderating and hosting the webinar on their platform, and delivering a lead list after the event. The project owner is responsible for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date.*

### How to request Partner Marketing support

CAMs and Alliance Managers should use this [issue template](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new) to request Channel Marketing support for their planned event or webinar.

#### How to view triage board of Partner Marketing Requests
{: #partner-hosted-webinar-triage-board}
<!-- DO NOT CHANGE THIS ANCHOR -->

[View Board](https://gitlab.com/groups/gitlab-com/-/boards/1779611?label_name[]=Channel&label_name[]=Channel%20Marketing)

### Process in GitLab to organize epic & issues
{: #partner-hosted-webinar-project-management}
<!-- DO NOT CHANGE THIS ANCHOR -->
The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner creates the epic to house all related issues (code below)
1. Project owner creates the relevant issues required (shortcut links in epic code below)
1. Project owner associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Epic code and issue templates
{: #partner-hosted-webinar-epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->
```
<!-- Name this epic: Channel Webinar - [Webinar Name] (Partner) - [3-letter Month] [Date], [Year] -->
<!-- Example epic name: Channel Webinar - Modern CI/CD with Anthos (WWT) - Apr 22, 2021 -->

## [Partner Marketing Request Issue >>]() `to be added`

- [ ] Once date is finalized, add to [All Marketing Calendar](https://docs.google.com/spreadsheets/d/1c2V3Aj1l_UT5hEb54nczzinGUxtxswZBhZV8r9eErqM/edit?usp=sharing)

## :notepad_spiral: Key Details 
* **Slack channel:** <!-- add slack channel # -->
* **[Meeting Notes]()** <!-- to be added by Partner Marketing -->
* **Speaker(s) and Moderator:** <!-- add gitlab handle and company -->
* **CAM:** <!-- add gitlab handle -->
* **Partner Marketing DRI:** <!-- add gitlab handle -->
* **Other GitLab Sponsors:** <!-- add gitlab handle(s) --> 
* **Partner's Marketing Liaison:** <!-- add gitlab handle / name -->
* **Marketo Program Type:** Webinar (Partner)
* **Organizer/Webinar hosting:** <!-- add who is hosting the webinar -->
* **Landing Page/Registration URL:** 
* **Persona (choose one):** `Practitioner, Manager, or Executive`
* **Sales Segment:** `Large` (GCP's focus)
* **Sales Region:** `AMER, EMEA, APAC`
* **Topic:** 
* **Event Name:** <!-- official name TBD -->
* **Event Date:** YYYY-MM-DD
* **Duration:**  (X min content + X min Q/A)
* **GTM Motion (choose primary):** `CI/CD, DevOps Platform, GitOps` 
* **Vertical (optional, if specific):** 
* **Goal:** `Please be specific on the metric this is meant to impact.`
* **Total Cost:** 
* **MDF Requested:** 

## Program Tracking <!-- Delete if not receiving leads -->
* [ ] [main SFDC campaign](tbd)
* [ ] [main Marketo program](tbd)
* [ ] [List clean and upload issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list)

## Existing Material/Assets/Presentations
- [Name of content]()

## :books: Tasks and Issues Created and Linked to Epic

## If GitLab is hosting, follow [Webcast process in handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#partner-webcasts

## General Checklist
- [ ] Make sure partner is adhering to the GitLab Branding guides and logo usage
- [ ] Provide opt in language to partner
- [ ] Review and approve all email and landing page copy
- [ ] Consider creating separate invite htmls with UTMs for each partner and GitLab to send to GitLab and the partner sales orgs to help drive attendance.  Separate UTMs will help us track registrations coming from each sales org
* [ ] Add to /events/ page - [Handbook Instructions](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)
* [ ] [Speaker request issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request) (optional)
* [ ] [Organic social promotion issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=social-gtm-organic) (optional)
* [ ] [Paid digital promotion issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) (optional)

## If partner is sharing leads:
- [ ] GitLab and partner to determine first touch and follow up
   - Best practice: joint lead follow up: Partner does 3 touches within 2 weeks following the event (a touch is a voicemail and email).  Qualified leads with immediate opportunity will be deal reg as they come in.  After 14 day follow up, full list with follow up notes will be provided to GitLab for list upload and further nurturing by GL
- [ ] [Program tracking issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-program-tracking)
- [ ] [List clean & upload request issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=event-clean-upload-list). (In lead upload, record the opt-in T&Cs used in the upload Issue and in the upload template, set Opt-in = True.
* [ ] [Add to nurture issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-add-nurture)

## If receiving recording and have distribution of webinar:
* [ ] [Pathfactory upload issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) (only open if we receive and can use recording, webinar DRI responsible for upload)
* [ ] [Pathfactory track issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) (only open if we receive and can use recording)

## Post event tasks:
- [ ] [Ordering a swag appreciation gift](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/community-appreciation/) for speakers
- [ ] [Request add to Resources](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition)

#### Opt in language applicable to all scenarios below and all geographies:
_By registering for this GitLab and [partner name] event, you agree that GitLab and [Partner name] may email you about their products, services and events. You may opt-out at any time by unsubscribing in future emails or visiting the relevant company's preference center._

In order to mark leads as Opt-in = TRUE, a record of the terms and conditions the leads agreed to upon having their data collected must be recorded. Check the terms of service wording has been recorded in the upload issue before opting in leads to receive marketing communications. No ToS, no Opt-in. Period. To find the appropriate language, refer to Marketing Rules and Consent Language
If there are any records who have opted out of contact for any reason, define that on the spreadsheet by selecting Opt-in = FALSE
Leave Opt-In empty if no other option is available

/label ~"mktg-status::wip" ~"Webinar - Channel Partner"  ~"Webinar"

```
