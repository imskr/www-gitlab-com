---
layout: job_family_page
title: "People Group Fullstack Engineering"
---

## People Group Fullstack Engineering Roles at GitLab
Fullstack Engineers in the People Group will work on all People Group related automation of various workflows, including working on GitLab backend (Ruby on Rails), and frontend (Vue.js).
Due to the nature of the work and projects involved in People Group, this role will require automating manual or semi-automated workflows across various other platforms used by the People Group. This position reports to the Senior Manager, People Operations.


<a id="intermediate-requirements"></a>
### Responsibilities

- Collaborate with all team members in People Group, as well as Engineering and Product to work on automation between various systems and creating smaller improvements inside of GitLab.
- Advocate for improvements to People Group projects, issue tracker and project in GitLab-com.
- Improve and automate our onboarding, career mobility and offboarding process, to ensure a compliant and streamlined workflow.
- Improve the onboarding process for new team members
- Automation of various manual tasks.
- API building for external systems (payroll, benefits, etc).
- Create tooling where needed to optimize workflows or processes
- Involvement in Compensation Calculator Design/updates.
- Architect and implement People Group support tool to streamline questions from multiple sources.
- Auditing People Group processes/iterating for efficiency.
- Automate updates to the about.gitlab.com/jobs page and maintain the job page
- Improvement and automation of contract creation.
- Provide and/or assist with data to the Data Analyst to be used in the People Group dashboards.
- Help the People Group maintain Single Source of Truth within the handbook
- Keep confidentiality in mind with all the projects and automations and actively look out that other People Group projects do this as well
- Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
- Represent GitLab and its values in public communication around specific projects and community contributions.


### Requirements
- Sincere interest in working on People Group related Engineering tasks.
- Professional experience with Ruby and Rails.
- Professional experience with JavaScript and associated web technologies (CSS, semantic HTML).
- Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment.
- Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions.
- Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems.
- Comfort working in a highly agile, intensely iterative software development process.
- Demonstrated ability to onboard and integrate with an organization long-term.
- Positive and solution-oriented mindset.
- Effective communication skills: Regularly achieve consensus with peers, and clear status updates.
- An inclination towards communication, inclusion, and visibility.
- Experience owning a project from concept to production, including proposal, discussion, and execution.
- Self-motivated and self-managing, with strong organizational skills.
- Demonstrated ability to work closely with other parts of the organization.
- Share our values, and work in accordance with those values.
- Ability to thrive in a fully remote organization.
- Ability to use GitLab

### Nice-to-haves

- Experience working with modern Frontend frameworks (eg. React, Vue.js, Angular).
- Experience in a peak performance organization, preferably a tech startup.
- Experience with the GitLab product as a user or contributor.
- Experience working with a remote team.
- Enterprise software company experience.
- Experience working with a global or otherwise multicultural team.
- Passionate about/experienced with open source and developer tools and how we use it in non-engineering teams.

## Levels

### People Group Fullstack Engineer

#### People Group Fullstack Engineer Job Grade
The People Group Fullstack Engineer is [Job Grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


#### People Group Fullstack Engineer Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 30 minute interview with our People Operations Manager.
- Candidates will then take part in a 90 minute technical interview with a peer from the team.
- Candidates will then have two 25 minute meetings with members from the People Operations and Compensation teams.
- Finally, candidates will be invited to a 25 minute interview with the Senior Director of People Success.
Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

### Senior People Group Fullstack Engineer

#### Senior People Group Fullstack Engineer Job Grade
The Senior People Group Fullstack Engineer is [Job Grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

#### Senior People Group Fullstack Engineer Responsibilities
- Advocate for improvements to quality, security, and performance that have particular impact across the People Group. 
- Solve technical problems of high scope and complexity.
- Exert influence on the overall objectives and long-range goals of the projects within the People Group.
- Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems.
- Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
- Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions.
- Provide mentorship for Junior and Intermediate Engineers and non-engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
- Confidently ship moderately sized features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.
- Improves the People Group engineering projects at GitLab via maintainer trainee program at own comfortable pace, while striving to become a project maintainer.
- Confidently ship moderately sized features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

#### Senior People Group Fullstack Engineer Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 30 minute interview with our People Operations Manager.
- Candidates will then take part in a 90 minute technical interview with a peer from the team.
- Candidates will then have two 25 minute meetings with members from the People Operations and Compensation teams.
- Finally, candidates will be invited to a 25 minute interview with the Senior Director of People Success.
Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

### Staff People Group Fullstack Engineer

#### Staff People Group Fullstack Engineer Job Grade
The Staff People Group Fullstack Engineer is [Job Grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff People Group Fullstack Engineer Responsibilities: 
WIP (and pulling inspiration from: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/job-families/engineering/backend-engineer/index.html.md#staff-backend-engineer)

#### Staff People Group Fullstack Engineer Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 30 minute interview with our People Operations Manager.
- Candidates will then take part in a 90 minute technical interview with a peer from the team.
- Candidates will then have two 25 minute meetings with members from the People Operations and Compensation teams.
- Candidates will then have a 25 minute meeting with an Engineering Manager.
- Finally, candidates will be invited to a 25 minute interview with the Senior Director of People Success.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

### Director, People Tools and Technology
The Director, People Tools and Technology reports to VP, People Operations.

#### Director, People Tools and Technology Job Grade
The Director, People Group Tools and Technology is [Job Grade 10](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, People Tools and Technology Responsibilities: 
- Collaborate across GitLab to implement various systems to improve scalability and efficiency.
- Create and execute a 3-Year technology roadmap.
- Create and implement a data analytics roadmap, creating data elements needed to support decision making and to create business insights.
- Improve and automate our onboarding, career mobility and offboarding processes in particular to ensure streamlined workflows and compliance. 
- Implement tools that improve the Team Member experience at GitLab.
- Ensure seamless data sharing from single sources of truth (Human Capital Management System) to external systems such as payroll, benefits, Okta.
- Implement People Group support tool to streamline questions from multiple sources.
- Implement People Group processes/iterating for efficiency.
- Create and implement data dictionary for consistent use of People data.  Implement data requirements and audits to ensure accuracy.  
- Help the People Group maintain Single Source of Truth within the handbook
- Keep confidentiality in mind with all the projects and automations and actively look out that other People Group projects do this as well
- Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
- Represent GitLab and its values in public communication around specific projects and community contributions.

#### Director, People Tools and Technology Requirements: 

- Ten+ years’ experience in People Technology, with seven+ years' experience leading and managing a People Technology organization or Applications group at a fast growing high technology company.
- Excellent change management skills and ability to operate effectively in a fast-paced environment.
- Experience with SaaS based solution and cloud architectures.
- Demonstrated ability to successfully lead and develop global People applications team in collaboration with overall People strategy.
- Knowledgeable in compliance and information security.
- Current on business applications, trends in People technology and experience evaluating/implementing new innovative People solutions
- A process-oriented individual, with substantial project management skills, who has the flexibility to thrive in a fast-paced, dynamic organization.
- Candidate must have proven ability to consistently and collaboratively resolve issues, mitigate roadblocks, and meet all financial and management goals on time. Will be a self-starter who maps own direction to succeed.
- Excellent team management, coaching, and mentoring skills
- Must be comfortable with ambiguity and fast change with an ability to adapt quickly and easily.
Relevant business/industry acumen with the ability to quickly and thoroughly understand business priorities, operations, and People enablement potential.
- Ability to use GitLab

#### Director, People Tools and Technology Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

Selected candidates will be invited to schedule a screening call with our Global Recruiters
Next, candidates will be invited to schedule a first interview with our VP, People Operations
Candidates will then be invited to schedule a second interview with our CPO, Sr. Director, People Experience, Sr. Director, IT Enterprise Applications

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

#### Director, People Tools and Technology Career Ladder
The next step is to move to VP, People Operations. 

## Performance Indicators
- [Narrow MR Rate](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-narrow-mr-rate)
- [Overall Handbook Update Frequency Rate](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-overall-handbook-update-frequency-rate)
- [Bug to first action](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-bug-to-first-action)
- [Workscope done within a milestone](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-workscope-done-within-a-milestone)
- [New requests are triaged](/handbook/people-group/people-success-performance-indicators/#people-group-engineering-new-requests-are-triaged)

## Career Ladder
The next step for both individual contributors and managers of people is to move to the [Engineering Leadership](/job-families/engineering/engineering-management) job family.
